package com.example.popup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity() {
    lateinit var nameText:TextView
    lateinit var addressText:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

         nameText = findViewById(R.id.nameTextView)
         addressText = findViewById(R.id.addressTextView)
        retrieveData()
    }

    private fun retrieveData() {
        val myPreference = getSharedPreferences("myPre", MODE_PRIVATE)

        val name = myPreference?.getString("name","")
        val address = myPreference?.getString("address","")
        nameText.text = name
        addressText.text = address
    }
}