package com.example.popup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var nameEditText: EditText
    lateinit var addressEditText: EditText
    lateinit var nameText:TextView
    lateinit var addressText:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nameEditText = findViewById(R.id.editTextName)
        addressEditText = findViewById(R.id.editTextAddress)
        nameText = findViewById(R.id.nameTextView)
        addressText = findViewById(R.id.addressTextView)


        val saveButton = findViewById<Button>(R.id.saveButton)
        saveButton.setOnClickListener {

//            val intent = Intent(this,DataActivity::class.java)
//            startActivity(intent)
            saveData()
            retrieveData()
        }

   }




    private fun saveData() {
        if (nameEditText.text.isEmpty()){
            editTextName.error = " Please Enter Name!"
            return
        }

        if (addressEditText.text.isEmpty()){
            editTextAddress.error = "Please Enter your City"
            return
        }

        val myPreference = getSharedPreferences("myPre", MODE_PRIVATE)
        val editor = myPreference.edit()
        editor.putString("name",nameEditText.text.toString())
        editor.putString("address",addressEditText.text.toString())
        editor.apply()

    }

    private fun retrieveData() {
        val myPreference = getSharedPreferences("myPre", MODE_PRIVATE)

        val name = myPreference?.getString("name","")
        val address = myPreference?.getString("address","")
        nameText.text = name
        addressText.text = address
    }
}




